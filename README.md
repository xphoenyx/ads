## ADS - add/delete site
A Bash script to manage your apache2 virtual hosts. 
#### NB: Script is quite raw and tested only on Ubuntu.

### Usage (command line)
#### NB: Execute only using `sudo`!

* Download the source, go to `ads` folder
* Initialize `install.sh` 
* Use `sudo addh` to add virtual host
* Use `sudo delh` to delete virtual host

Keep an eye on console output and always remember to execute both commands in your server root (where all your projects are)

## A brief list of actions to quickly install apache2 web server on your local machine

* Install required packages: `sudo apt-get install apache2 mysql-server php5 php5-mysql php5-mcrypt`
* Paste this line at the end of `/etc/apache2/apache2.conf` file: `ServerName localhost`
* If you have server root folder different from `/var/www/`, edit path of `<Directory /var/www/>` in `/etc/apache2/apache2.conf`

Optional:

* Enable apache2 rewrite module: `sudo a2enmod rewrite`
* Test MySQL server if it works: `mysql -u<mysql_username_here> -p`
* Install git core: `sudo apt-get install git-core`
* Install MySQL workbench