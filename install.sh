#!/bin/bash

chmod +x addh delh;
cp addh delh /usr/local/bin/;
cp conf-template /etc/apache2/sites-available/;
chown root:root /etc/apache2/sites-available/conf-template;
chmod 644 /etc/apache2/sites-available/conf-template;
echo "Installed successfully.";